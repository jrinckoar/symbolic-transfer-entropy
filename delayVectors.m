function V = delayVectors(x, m, tau)
%DELAYVECTORS Build the m-dimensional time-delay vectors.
% For reference see: https://arxiv.org/abs/1903.07720
%
%
% Syntax: V = delayVectors(x, m, tau)
%
% Inputs:
%   x -- Series
%   m -- Dimension of the delay vector
%   tau -- Time-delay
%
% Outputs:
%   V -- Delay-vectors
%
% $Date: 2019-03-21
%
% $Author: Juan F. Restrepo, Ph.D.
%
% Copyright (c) 2019, Juan F. Restrepo
% All rights reserved.

L=length(x)-(m-1)*tau;
V=zeros(L,m);
k=(m-1)*tau;
for i=1:L
    V(i,:)=x(i:tau:i+k);
end
