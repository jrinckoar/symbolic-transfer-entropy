function [T, tyx, txy] = symbolictent(x, y, m, tau)


% normalize time series to zero mean and unitary standard deviation
x = (x - mean(x)) ./ std(x);
y = (y - mean(y)) ./ std(y);

% Time-embedd vectors
Vx = delayVectors(x, m, tau); % (X^(m)_t-tau)
Vy = delayVectors(y, m, tau); % (Y^(m)_t-tau)

% sort coordinates of time-embeed vectors
[~, Vxs] = sort(Vx, 2);
[~, Vys] = sort(Vy, 2);
% Get permutation sequences
Nperm = factorial(m);
permutations = flipud(perms(1:m));
% symbols sequences
N = length(Vxs);
xs = zeros(N, 1);
ys = zeros(N, 1);
inds = (1:N)';
for j=1:Nperm
    indx = nonzeros((prod(Vxs == permutations(j,:), 2)).*inds);
    indy = nonzeros((prod(Vys == permutations(j,:), 2)).*inds);
    xs(indx) = j;
    ys(indy) = j;
end

% Obtain commun joint probabilities
prob_x = zeros(Nperm, 1);                   % prob(x=x_i)
prob_y = zeros(Nperm, 1);                   % prob(y=y_i)
prob_x_y = zeros(Nperm, Nperm);             % prob(x=x_i, y=y_i)


prob_xt_x_y = zeros(Nperm, Nperm, Nperm);   % prob(x_tau+=x_i+tau, x=x_i, y=y_i)
prob_xt_x = zeros(Nperm, Nperm);            % prob(x_tau=x_i+tau, x=x_i)

prob_yt_y_x = zeros(Nperm, Nperm, Nperm);   % prob(y_tau+=y_i+tau, y=y_i, y=x_i)
prob_yt_y = zeros(Nperm, Nperm);            % prob(y_tau=y_i+tau, y=y_i)


for i=1:N-tau
    % Obtain commun joint probabilities
    prob_x_y(xs(i),ys(i)) = prob_x_y(xs(i),ys(i)) + 1;
    prob_x(xs(i)) = prob_x(xs(i)) + 1;
    prob_y(ys(i)) = prob_y(ys(i)) + 1;

    % joint probabilities for tyx
    prob_xt_x_y(xs(i+tau), xs(i), ys(i)) = ...
        prob_xt_x_y(xs(i+tau), xs(i), ys(i)) + 1;
    prob_xt_x(xs(i+tau), xs(i)) = prob_xt_x(xs(i+tau),xs(i)) + 1;

    % joint probabilities for txy
    prob_yt_y_x(ys(i+tau), ys(i), xs(i)) = ...
        prob_yt_y_x(ys(i+tau), ys(i), xs(i)) + 1;
    prob_yt_y(ys(i+tau), ys(i)) = prob_yt_y(ys(i+tau),ys(i)) + 1;
end
prob_x_y = prob_x_y(:) ./ (N - tau);
prob_x = prob_x(:) ./ (N - tau);
prob_y = prob_y(:) ./ (N - tau);
prob_xt_x_y = prob_xt_x_y(:) ./ (N-tau);
prob_xt_x = prob_xt_x(:) ./ (N-tau);
prob_yt_y_x = prob_yt_y_x(:) ./ (N-tau);
prob_yt_y = prob_yt_y(:) ./ (N-tau);

%% Transfer entropy x-target and y-source - tyx

% H(x_t, x, y)

prob_xt_x_y(prob_xt_x_y==0) = 1;
H0 = -prob_xt_x_y' * log(prob_xt_x_y);

% H(x, y)
prob_x_y(prob_x_y==0) = 1;
H1 = -prob_x_y' * log(prob_x_y);

% H(xt, x)
prob_xt_x(prob_xt_x==0) = 1;
H2 = -prob_xt_x' * log(prob_xt_x);

% H(x)
prob_x(prob_x==0) = 1;
H3 = -prob_x' * log(prob_x);

tyx = H1 - H0 + H2 - H3;

%% Transfer entropy x-target and y-source - tyx

% H(y_t, y, x)
prob_yt_y_x(prob_yt_y_x==0) = 1;
H0 = -prob_yt_y_x' * log(prob_yt_y_x);

% H(yt, y)
prob_yt_y(prob_yt_y==0) = 1;
H2 = -prob_yt_y' * log(prob_yt_y);

% H(y)
prob_y(prob_y==0) = 1;
H3 = -prob_y' * log(prob_y);

txy = H1 - H0 + H2 - H3;

T = tyx - txy;
