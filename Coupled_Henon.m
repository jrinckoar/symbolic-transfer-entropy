function [x, y] = Coupled_Henon(N, e)
%COUPLED_HENON Simulates the Henon-Henon coupled system.
% System Y drives system X
% For reference see: https://arxiv.org/abs/1903.07720
%
%
% Syntax: V = CoupledHenon(N, e)
%
% Inputs:
%   N -- Length of the time series
%   e -- Coupling parameter
%
% Outputs:
%   x -- x-coordinate time series
%   y -- y-coordinate time series
%
% $Date: 2019-03-21
%
% $Author: Juan F. Restrepo, Ph.D.
%
% Copyright (c) 2019, Juan F. Restrepo
% All rights reserved.

y = zeros(N + 10000,1);
x = zeros(N + 10000,1);
% Random initial contion
y(1:2) = rand(2,1) * 0.1;
x(1:2) = rand(2,1) * 0.1;

flag = 1;
while flag
  for i=3: N + 10000
    y(i) = 1.4 - y(i-1)^2 + 0.3 * y(i-2);
    x(i) = 1.4 - (e * y(i-1) + (1-e) * x(i-1)) * x(i-1) + 0.3 * x(i-2);
  end
  if sum(isinf(x) +  isinf(y)) > 0    % check if solutions are bounded
    disp('inf')
    y(1:2) = rand(2,1);
    x(1:2) = rand(2,1);
  else
    flag = 0;
  end
end
% Remove the 10000 first data points
y = y(10000:end-1);
x = x(10000:end-1);
