% Example Transfer entropy rate -- Lemplel-Ziv approach
% Boxplot of the Global Transfer Entropy Rate vs the coupling parameter
% For reference see: https://arxiv.org/abs/1903.07720
clear;
clc;

REA = 20;  % Number of realizations of the coupled Henon-Henon map
T = 5000;   % Length of the time series
EPS = linspace(0, 1, 11); % Values for the coupling parameter
m = 3; % embedding-dimention / history-length
tau = 1; % emedding-lag / time-delay

tentropy = zeros(REA, length(EPS));
for i =1:REA
    parfor  j=1:length(EPS)
        [x, y] = Coupled_Henon(T, EPS(j));
        tentropy(i, j) = symbolictent(x, y, m, tau);
    end
end

figure;boxplot(tentropy, EPS);
xlabel('\boldmath$\epsilon$','Interpreter','Latex')
ylabel('\boldmath$\mathcal{T}$','Interpreter','Latex')
